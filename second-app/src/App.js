import React, {Component} from "react"

class App extends Component {
    constructor() {
        super()
        this.state = {
            firstName: "",
            lastName: "",
            age: "",
            gender: "",
            destination: "",
            cricket: false,
            football: false,
            tennis: false
        }
        this.handleChange = this.handleChange.bind(this)
    }
    
    handleChange(event) {
        const {name, value, type, checked} = event.target
        type === "checkbox" ? 
            this.setState({
                [name]: checked
            })
        :
        this.setState({
            [name]: value
        }) 
    }
    
    render() {
        return (
            <main>
                <form>
                    <input 
                        name="firstName" 
                        value={this.state.firstName} onChange={this.handleChange} 
                        placeholder="First Name" 
                    /> <br />
                    
                    <input 
                        name="lastName" 
                        value={this.state.lastName} onChange={this.handleChange} 
                        placeholder="Last Name" 
                    /> <br />
                    
                    <input 
                        name="age" 
                        value={this.state.age} onChange={this.handleChange} 
                        placeholder="Age" 
                    /> <br />
                   
                    <label>
                        <input 
                            type="radio"  name="gender"
                            value="male"  checked={this.state.gender === "male"}
                            onChange={this.handleChange}
                        /> Male
                    </label> <br />                    
                   
                    <label>
                        <input 
                            type="radio" name="gender"
                            value="female" checked={this.state.gender === "female"}
                            onChange={this.handleChange}
                        /> Female
                    </label> <br />

                    <select 
                        value={this.state.destination} 
                        name="destination" 
                        onChange={this.handleChange}
                    >
                        <option value="">Choose option</option>
                        <option value="germany">Bangalore</option>
                        <option value="norway">Delhi</option>
                        <option value="north pole">Mumbai</option>
                        <option value="south pole">Chennai</option>
                    </select> <br />                   
                    
                    <label>
                        <input 
                            type="checkbox"
                            name="cricket"
                            onChange={this.handleChange}
                            checked={this.state.cricket}
                        /> Cricket?
                    </label> <br />
                    
                    <label>
                        <input 
                            type="checkbox"
                            name="football"
                            onChange={this.handleChange}
                            checked={this.state.football}
                        /> Football?
                    </label>
                    <br />
                    
                    <label>
                        <input 
                            type="checkbox"
                            name="tennis"
                            onChange={this.handleChange}
                            checked={this.state.tennis}
                        /> Tennis?
                    </label>
                    <br />
                    
                    <button>Submit</button>
                </form>
                <hr />
                <h2>Entered information:</h2>
                <p>Your name: {this.state.firstName} {this.state.lastName}</p>
                <p>Your age: {this.state.age}</p>
                <p>Your gender: {this.state.gender}</p>
                <p>Your destination: {this.state.destination}</p>
                <p>Games You Like</p>                
                <p>Cricket: {this.state.cricket ? "Yes" : "No"}</p>
                <p>Football: {this.state.football ? "Yes" : "No"}</p>
                <p>Tennis Free: {this.state.tennis ? "Yes" : "No"}</p>
                
            </main>
        )
    }
}

export default App
